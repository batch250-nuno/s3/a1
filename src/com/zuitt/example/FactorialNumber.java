package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int num = 0;
        int answer = 1;

        System.out.println("Enter a Number to Factorial: ");

        try {
            num = in.nextInt();
            if (num < 0) {
                throw new IllegalArgumentException("Invalid input : Must not be Negative Value" );
            }
            for (int counter = 1; counter <= num; counter++) {
                answer *= counter;
            }
            System.out.println("The Factorial of " + num + " is " + answer);
        }

        catch (Exception e) {
            System.out.println("Only Positive Numbers & no Letters Please!");
            e.printStackTrace();
        }

        finally {
            System.out.println("You have Entered: " + num);
        }
    }
}
