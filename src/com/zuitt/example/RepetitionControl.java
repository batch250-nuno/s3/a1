package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {

    public static void main(String[] args) {
        //Loops
        //While Loop

        //int x = 0;
//        while (x < 10) {
//            System.out.println("Loop number: " + x);
//            x++;
//        }

        //Do-While Loop
//        int y = 10;
//        do {
//            System.out.println("Countdown: " + y);
//            y--;
//        } while (y >= 0);

        //For Loop
//        for (int i = 0; i < 10; i++) {
//            System.out.println("Current count: " + i);
//        }

        //For Loop with Array
//        int[] intArray = {100, 200, 300, 400, 500};
//
//        for (int i = 0; i < intArray.length; i++) {
//            System.out.println(intArray[i]);
//        }

        //For-each Loop with Array
//        String[] nameArray = {"John", "Paul", "George", "Ringo"};
//        for (String name : nameArray) {
//            System.out.println(name);
//        }

        //Nested Loops with Multidimensional
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Portholes";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Robert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //For Loop with Multidimensional
        //Outer Loop
//        for (int row = 0; row < classroom.length; row++) {
//            //Inner Loop
//            for (int col = 0; col < classroom.length; col++) {
//                System.out.println("classroom[" + row + "][" + col + "] = " + classroom[row][col]);
//            }
//        }

        //For-each with Multidimensional
        //Row / Outer Loop
        for (String[] row : classroom) {
            //Col / Inner Loop
            for (String col: row) {
                System.out.println(col);
            }
        }

        //For-each Loop with ArrayList
//        ArrayList<Integer> numbers = new ArrayList<>();
//        numbers.add(5);
//        numbers.add(10);
//        numbers.add(15);
//        numbers.add(20);
//        numbers.add(25);
//
//        System.out.println("ArrayList: " + numbers);
//        numbers.forEach(num -> System.out.println("ArrayList: " + num));

        //For-each Loop with HashMap
//        HashMap<Object, Object> grades = new HashMap<Object, Object>() {
//            {
//                put("English", 90);
//                put("Math", 95);
//                put("Science", 97);
//                put("History", 94);
//            }
//        };
//
//        grades.forEach((subject, grade) -> System.out.println(subject + " : " + grade));

    }

}
