package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        //Exceptions
        //Exception Handling
            //Compile Error
            //Run-time Error

        Scanner input = new Scanner(System.in);

        int num = 0;

        System.out.println("Please enter a number: ");
        //try
        try {
            num = input.nextInt();
//            System.out.println("You enter a number " + num);
        }
        //catch
        catch (Exception e) {
            System.out.println("Invalid Input. Please enter a number.");
            //prints the throwable error
            e.printStackTrace();
        }

        //optional block
        //finally
        finally {
            System.out.println("You have Entered: " + num);
        }

//        num = input.nextInt();
        System.out.println("Hello World!");

    }
}
